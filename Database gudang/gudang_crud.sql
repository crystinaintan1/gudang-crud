CREATE DATABASE  IF NOT EXISTS `stock_barang_crud` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `stock_barang_crud`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: stock_barang_crud
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `barang`
--

DROP TABLE IF EXISTS `barang`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `barang` (
  `idBarang` int(11) NOT NULL AUTO_INCREMENT,
  `namaBarang` varchar(150) NOT NULL,
  `idKategori` int(11) NOT NULL,
  `deskripsi` varchar(350) NOT NULL,
  `stok` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`idBarang`),
  KEY `idKategori_idx` (`idKategori`),
  CONSTRAINT `idKategori` FOREIGN KEY (`idKategori`) REFERENCES `kategori` (`idKategori`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `barang`
--

LOCK TABLES `barang` WRITE;
/*!40000 ALTER TABLE `barang` DISABLE KEYS */;
INSERT INTO `barang` VALUES (2,'cadburry',2,'Ini coklat cc',5,'2019-07-25 13:32:14','2019-07-25 13:32:14',1),(4,'Choco',2,'Ini coklat yang berbentuk bulat.',10,'2019-07-25 13:27:34','2019-07-25 13:27:34',1),(5,'cadburry choco',2,'Ini coklat yang berbentuk panjang dan rasanya enakkkk.',-7,'2019-07-23 09:33:45','2019-07-25 09:46:24',0),(6,'HP',11,'Ini laptop yang bagus.',1,'2019-07-23 10:40:23','2019-07-23 10:40:23',1),(7,'asus',11,'Ini laptop.',3,'2019-07-25 10:20:58','2019-07-25 10:22:09',1);
/*!40000 ALTER TABLE `barang` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `history`
--

DROP TABLE IF EXISTS `history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `history` (
  `idHistory` int(11) NOT NULL AUTO_INCREMENT,
  `idBarang` int(11) NOT NULL,
  `kode` char(1) NOT NULL,
  `jumlahBarang` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  PRIMARY KEY (`idHistory`),
  KEY `idBarang_idx` (`idBarang`),
  CONSTRAINT `idBarang` FOREIGN KEY (`idBarang`) REFERENCES `barang` (`idBarang`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `history`
--

LOCK TABLES `history` WRITE;
/*!40000 ALTER TABLE `history` DISABLE KEYS */;
INSERT INTO `history` VALUES (1,5,'I',8,'2019-07-23 09:33:45'),(2,4,'O',6,'2019-07-23 09:46:44'),(3,6,'I',1,'2019-07-23 10:40:24'),(4,4,'O',5,'2019-07-23 10:46:46'),(5,5,'O',5,'2019-07-23 13:24:06'),(6,5,'O',5,'2019-07-23 13:24:34'),(7,2,'I',1,'2019-07-25 09:04:23'),(8,2,'I',6,'2019-07-25 09:06:58'),(9,5,'O',5,'2019-07-25 09:07:34'),(10,2,'O',5,'2019-07-25 09:31:38'),(11,2,'O',5,'2019-07-25 09:32:46'),(12,2,'O',3,'2019-07-25 09:34:36'),(13,2,'I',10,'2019-07-25 09:36:38'),(14,7,'I',13,'2019-07-25 10:20:59'),(15,7,'O',10,'2019-07-25 10:22:09'),(16,4,'I',10,'2019-07-25 13:27:34'),(17,2,'I',5,'2019-07-25 13:32:14');
/*!40000 ALTER TABLE `history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kategori`
--

DROP TABLE IF EXISTS `kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `kategori` (
  `idKategori` int(11) NOT NULL AUTO_INCREMENT,
  `namaKategori` varchar(100) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`idKategori`),
  UNIQUE KEY `namaKategori_UNIQUE` (`namaKategori`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kategori`
--

LOCK TABLES `kategori` WRITE;
/*!40000 ALTER TABLE `kategori` DISABLE KEYS */;
INSERT INTO `kategori` VALUES (1,'sabun','2019-07-22 12:32:13','2019-07-22 14:16:00',1),(2,'coklat7','2019-07-22 14:13:40','2019-07-25 13:24:48',1),(3,'pakaian anak','2019-07-22 14:22:03','2019-07-22 14:35:27',1),(6,'permen','2019-07-22 14:38:34','2019-07-23 11:23:36',1),(7,'boneka','2019-07-22 14:41:11','2019-07-23 11:23:48',1),(9,'tas','2019-07-22 14:45:11','2019-07-22 14:45:11',1),(10,'jaket','2019-07-22 14:46:18','2019-07-22 14:46:18',1),(11,'sendal','2019-07-23 10:26:38','2019-07-23 11:24:00',1),(12,'HandPhone','2019-07-23 10:27:25','2019-07-23 12:17:14',0),(14,'laptop','2019-07-23 10:28:46','2019-07-23 10:28:46',1),(19,'makanan','2019-07-23 12:12:48','2019-07-23 12:18:45',1);
/*!40000 ALTER TABLE `kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'stock_barang_crud'
--
/*!50003 DROP PROCEDURE IF EXISTS `insertHistory` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertHistory`(barangId INT, kodeHistory CHAR, jumlah INT)
BEGIN
	INSERT INTO stock_barang_crud.history (idBarang, kode, jumlahBarang, createdAt)
			VALUES (barangId, kodeHistory, jumlah, now());
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUpdateBarang` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUpdateBarang`(idChange INT, nameBarang varchar(1000), kategoriId INT, deskripsiBarang varchar(1000), 
	stokBarang INT, kode char)
BEGIN
	DECLARE idFind INT;
    DECLARE idKategoriFind INT;
    DECLARE stokIdFind INT;
    DECLARE hasilString varchar(100);
    DECLARE stokSekarang INT;
    SET stokSekarang = 0;
    SET idFind = 0;
    SET idKategoriFind = 0;
    SET stokIdFind = 0;
    SET hasilString = '';
    
	SELECT idBarang INTO idFind FROM stock_barang_crud.barang WHERE idBarang = idChange AND status=true;
    
    if idFind = 0
		THEN -- insert
        SELECT idKategori INTO idKategoriFind FROM stock_barang_crud.kategori WHERE idKategori = kategoriId AND status = true;
        
        if idKategoriFind = 0
			THEN
            SET hasilString = 'Masukan kategori terlebih dahulu.';
            SELECT hasilString;
		else
			SELECT idBarang INTO idFind FROM stock_barang_crud.barang WHERE namaBarang = nameBarang AND status = false;
            
            if idFind = 0
				THEN
                INSERT INTO stock_barang_crud.barang (namaBarang, idKategori, deskripsi, stok, createdAt, updatedAt, status)
					VALUES (nameBarang, kategoriId, deskripsiBarang, stokBarang, now(), now(), true);
				
				SELECT max(idBarang) INTO idFind FROM stock_barang_crud.barang;
			else
				UPDATE stock_barang_crud.barang 
					SET namaBarang = nameBarang, idKategori = kategoriId, deskripsi = deskripsiBarang, 
						stok = stokBarang, createdAt = now(), updatedAt = now(), status = true
					WHERE idBarang = idFind;
			END IF;
            
				-- call sp insert to history
				CALL `stock_barang_crud`.`insertHistory`(idFind, 'I', stokBarang);
            
            SET hasilString = 'Inserted successfully';
            SELECT *, hasilString FROM stock_barang_crud.barang WHERE idBarang = idFind;
		END IF;
	else
		if nameBarang IS NOT NULL
			THEN UPDATE stock_barang_crud.barang 
				SET namaBarang = nameBarang, updatedAt = now()
				WHERE idBarang = idFind;
		END IF;
        
        if deskripsiBarang IS NOT NULL
			THEN UPDATE stock_barang_crud.barang 
				SET deskripsi = deskripsiBarang, updatedAt = now()
				WHERE idBarang = idFind;
        END IF;        
		
        if kategoriId IS NOT NULL
			THEN UPDATE stock_barang_crud.barang 
				SET idKategori = kategoriId, updatedAt = now()
				WHERE idBarang = idFind;
			SELECT idFind;
		END IF;
        
        if (stokBarang IS NOT NULL) AND (kode IS NOT NULL)
			THEN SELECT stok INTO stokIdFind FROM stock_barang_crud.barang WHERE idBarang = idChange;
			
			if kode = 'I' OR kode = 'i'
				THEN SET stokIdFind = stokIdFind + stokBarang;
				
				UPDATE stock_barang_crud.barang 
					SET stok = stokIdFind, updatedAt = now()
					WHERE idBarang = idFind;
					
				-- call sp insert to history
				CALL `stock_barang_crud`.`insertHistory`(idFind, 'I', stokBarang);
			else
				if kode = 'O' OR kode = 'o'
					THEN SET stokSekarang = stokIdFind;
					SET stokIdFind = stokIdFind - stokBarang;
                    
                    if stokIdFind < 0
						THEN SET hasilString = 'Stok di gudang tidak cukup';
					else
						UPDATE stock_barang_crud.barang 
							SET stok = stokIdFind, updatedAt = now()
							WHERE idBarang = idFind;
							
							-- call sp insert to history
							CALL `stock_barang_crud`.`insertHistory`(idFind, 'O', stokBarang);
					END IF;
                else
					SET hasilString = 'masukan kode I atau O';
                END IF;
			END IF;
        END IF;
        
        if hasilString = ''
			THEN SET hasilString = 'Updated successfully';
		END IF;
        SELECT *, hasilString FROM stock_barang_crud.barang WHERE idBarang = idFind;
	END IF;   
end ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `insertUpdateKategori` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `insertUpdateKategori`(idChange INT, kategoriName varchar(10000))
BEGIN
	DECLARE idFind INT;
    DECLARE hasil INT;
    DECLARE hasilString varchar(100);
    DECLARE idCariNama INT;
    DECLARE statusKategori boolean;
    SET hasil = 0; -- 1 -> insert, 2 -> update
    SET idFind = 0;
    SET idCariNama = 0;
    
	SELECT idKategori INTO idFind FROM stock_barang_crud.kategori WHERE idKategori = idChange AND status = true;
    
    if idFind = 0
		THEN -- insert
        SELECT idKategori INTO idCariNama FROM stock_barang_crud.kategori WHERE namaKategori = kategoriName;
        
        if idCariNama != 0 
			THEN SELECT status INTO statusKategori FROM stock_barang_crud.kategori WHERE idKategori = idCariNama;
            
            if statusKategori = false
				THEN
				UPDATE stock_barang_crud.kategori 
					SET status = true, updatedAt = now()
					WHERE idKategori = idCariNama;
				SET hasil = 2;
				SET hasilString = 'Updated successfully';
			else
				SET hasilString = 'Nothing change';
			END IF;
            
            SET idFind = idcariNama;
		else
			INSERT INTO stock_barang_crud.kategori (namaKategori, createdAt, updatedAt, status)
				VALUES (kategoriName, now(), now(), true);
			SET hasil = 1;
			SET hasilString = 'Inserted successfully';
            
            SELECT max(idKategori) INTO idFind FROM stock_barang_crud.kategori;
        END IF;       
	else
        UPDATE stock_barang_crud.kategori 
			SET namaKategori = kategoriName, updatedAt = now()
			WHERE idKategori = idFind;
		SET hasil = 2;
        SET hasilString = 'Updated successfully';
	END IF;
    
    SELECT idFind AS id, kategoriName AS namaKategori, hasilString;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-25 13:33:12
