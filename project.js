const myqsl = require('mysql');
const express = require('express');
var app = express();
const jwt = require('jsonwebtoken');
const bodyparser = require('body-parser');
app.use(bodyparser.json());

var mysqlConnection = myqsl.createConnection({
    host: 'localhost',
    user:'root',
    // password:'12345678',
    password: 'root',
    database:'stock_barang_crud',
    multipleStatements: true
});

mysqlConnection.connect((err)=>{
    if(!err) console.log('DB connection succeded');
    else console.log('DB connection failed \n Error : '+ JSON.stringify(err, undefined, 2));
});

app.listen(4000,()=>console.log('Express server is running at port no : 4000'));

//login
app.post('/pergi_ke_gudang/login', (req,res) =>{
    // Mock User
    const user = {
        id: 1,
        username: 'admin',
        email: 'admin@gmail.com'
    }
    jwt.sign({user}, 'secretkey', /*{expiresIn: '30s'},*/ (err, token)=>{
        res.json({
            token
        });
    });

});

//fungsi untuk verifikasi
function verifyToken(req, res, next){
    const bearerHeader = req.headers['authorization'];
    if(typeof bearerHeader !== 'undefined')
    {
        //get token from req Header authorization 
        const bearer = bearerHeader;
        //set the token
        req.token = bearer;
        //middleware
        next();
    }
    else{
        //forbidden
        res.sendStatus(403);
    }
}

//get All listBarang
app.get('/pergi_ke_gudang_barang', verifyToken,(req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('SELECT * FROM barang WHERE status=true', (err, rows, fields)=>{
                if(!err) res.send(rows);
                else console.log(err);
            })
        }
    });
    
});

//get All listKategori
app.get('/pergi_ke_gudang_kategori', verifyToken,(req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('SELECT * FROM kategori WHERE status=true', (err, rows, fields)=>{
                if(!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//get All listHistory
app.get('/pergi_ke_gudang_History', verifyToken,(req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('SELECT * FROM history', (err, rows, fields)=>{
                if(!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//get any listBarang with id (read to do list)
app.get('/pergi_ke_gudang_barang/:id', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('SELECT * FROM barang where idBarang = ? AND status = true',[req.params.id], (err, rows, fields)=>{
                if(!err) res.send(rows);
                else console.log(err);
            });
        }
    });    
});

//get any listKategori with id (read to do list)
app.get('/pergi_ke_gudang_kategori/:id', verifyToken ,(req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('SELECT * FROM kategori where idKategori = ? AND status = true',[req.params.id], (err, rows, fields)=>{
                if(!err) res.send(rows);
                else console.log(err);
            });
        }
    });
});

//Delete to do listBarang
app.delete('/pergi_ke_gudang_barang/:id', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('UPDATE stock_barang_crud.barang SET status=false, updatedAt = now() WHERE idBarang = ?;',[req.params.id], (err, rows, fields)=>{
                if(!err) res.send('DELETE SUCCESSFULLY');
                else console.log(err);
            });
        }
    });
});

//Delete to do listKategori
app.delete('/pergi_ke_gudang_kategori/:id', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            mysqlConnection.query('UPDATE stock_barang_crud.kategori SET status=false, updatedAt = now() WHERE idKategori = ?;\
            UPDATE stock_barang_crud.barang SET status=false, updatedAt = now() WHERE idKategori = ?;' ,[req.params.id, req.params.id], (err, rows, fields)=>{
                if(!err) res.send('DELETE SUCCESSFULLY');
                else console.log(err);
            });
        }
    });
});

//insert barang
app.post('/pergi_ke_gudang_barang', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            let emp = req.body;
            var sql = "SET @idBarang = ?; SET @namaBarang = ?; SET @idKategori = ?; SET @deskripsi = ?; SET @stok = ?; SET @kode = 'I';\
                CALL insertUpdateBarang(@idBarang, @namaBarang, @idKategori, @deskripsi, @stok, @kode);";
            mysqlConnection.query(sql,[emp.idBarang, emp.namaBarang, emp.idKategori, emp.deskripsi, emp.stok],(err, rows, fields)=>{
            if(!err) 
                rows.forEach(element => {
                    if(element.constructor == Array)
                       if(element[0].hasilString == 'Masukan kategori terlebih dahulu.')
                       {
                            res.send(element[0].hasilString);
                       }
                       else
                       {
                            res.send(element[0].hasilString + '. id barang : ' + element[0].idBarang + ', nama barang = ' + element[0].namaBarang
                            + ', id kategori = ' + element[0].idKategori + ', deskripsi barang= ' + element[0].deskripsi + ', stok barang = ' + 
                            element[0].stok);
                       }

                });
            else console.log(err);
            });
        }
    });
});

//update barang
app.put('/pergi_ke_gudang_barang', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            let emp = req.body;
            var sql = "SET @idBarang = ?; SET @namaBarang = ?; SET @idKategori = ?; SET @deskripsi = ?; SET @stok = ?; SET @kode = ?;\
                CALL insertUpdateBarang(@idBarang, @namaBarang, @idKategori, @deskripsi, @stok, @kode);";
            mysqlConnection.query(sql,[emp.idBarang, emp.namaBarang, emp.idKategori, emp.deskripsi, emp.stok, emp.kode],(err, rows, fields)=>{
            if(!err) 
                rows.forEach(element => {
                    if(element.constructor == Array)
                        res.send(element[0].hasilString + '. id barang : ' + element[0].idBarang + ', nama barang = ' + element[0].namaBarang
                            + ', id kategori = ' + element[0].idKategori + ', deskripsi barang= ' + element[0].deskripsi + ', stok barang = ' + 
                            element[0].stok);
                });
            else console.log(err);
            });
        }
    });
});

//insert kategori
app.post('/pergi_ke_gudang_kategori', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            let emp = req.body;
            var sql = "SET @idKategori = ?; SET @namaKategori = ?; \
                CALL insertUpdateKategori(@idKategori, @namaKategori);";
            mysqlConnection.query(sql,[emp.idKategori, emp.namaKategori],(err, rows, fields)=>{
            if(!err) 
                rows.forEach(element => {
                    if(element.constructor == Array)
                        res.send(element[0].hasilString + '. id barang : ' + element[0].id + ' dan nama kategori = ' + element[0].namaKategori);
                });
            else console.log(err);
            });
        }
    });
});

//update kategori
app.put('/pergi_ke_gudang_kategori', verifyToken , (req,res)=>{
    jwt.verify(req.token, 'secretkey', (err)=>{
        if(err)
        {
            res.sendStatus(403);
        }
        else
        {
            let emp = req.body;
            var sql = "SET @idKategori = ?; SET @namaKategori = ?; \
                CALL insertUpdateKategori(@idKategori, @namaKategori);";
            mysqlConnection.query(sql,[emp.idKategori, emp.namaKategori],(err, rows, fields)=>{
            if(!err) 
                rows.forEach(element => {
                    if(element.constructor == Array)
                        res.send(element[0].hasilString + '. id barang : ' + element[0].id + ' dan nama kategori = ' + element[0].namaKategori);
                });
            else console.log(err);
            });
        }
    });
});